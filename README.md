
# Base de données

----------------------------------------------

----------------------------------------------

# **Notes pour le professeur:**

# Organisation:

Tous les scripts se trouvent dans les dossiers **public/** des branches.

# Installer les dépendances et l'autoloader (à la racine, là où le fichier **composer.json** se situe)

	composer install

# **Configurer** la base de données (username, mot de passe...)

	cp src/config/db.exemple.ini src/config/db.ini

	nano src/config/db.ini

----------------------------------------------

----------------------------------------------


Groupe SI2.

| Groupe                                     |
| -------------------------------------------|
| Vernevaut Corentin                         |
| Blaise Louis                               |
| Kimm Raphael                               |
| Hublau Alexandre                           |

----------------------------------------------

